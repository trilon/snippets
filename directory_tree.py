import subprocess
import os

import argparse
path_depth =0
indent = 4
indent_char='-'
    
def explore(path, depth, recursive = None):
    """
    printout the directory tree with indents
    """
    depth +=1
    path_content = os.listdir(path)
    #print(path_content)
    for elem in path_content:
        if elem is not None:
            item = os.path.join( path, elem) 
            if os.path.isfile(item):
                print( indent_char*depth*indent + elem )

            elif os.path.isdir( item ):
                print(indent_char*depth*indent + elem + '/')
                if recursive is not None:
                    child_path = os.path.join(path, item)
                    explore( child_path, depth, recursive)


def walker( path ):
    """
    printout directory tree full path
    """
    for root, dirs, files in os.walk( path , topdown=False):
       for name in files:
          print(os.path.join(root, name))
       for name in dirs:
          print( os.path.join(root, name))

######################################################################
def args():
    parser = argparse.ArgumentParser()
    parser.add_argument("path", type=str, help= "the path to explore" ) 
    parser.add_argument( '-r', "--recursive", help= "recursive mode" , action="store_true") 

    return parser.parse_args()
######################################################################
if __name__ == '__main__':
    print()
    opts = args()

    # walker( opts.path )


if opts.path is not None:
    print( opts.path +'/')
    pth = opts.path
    if opts.path == '.':
        pth = os.getcwd()

    explore( pth , path_depth, opts.recursive)