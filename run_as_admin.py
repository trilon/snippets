import ctypes, sys

"""
atttemps to run the current programm with elevated priviledges.
if current user is not an administrator, ask for admin password,
reruns the script in promt with elevated priviledges.

"""
def admin_only():
    print( 'hello')
    input('press enter to finish program')


def run_as_admin():
    def is_admin():
        try:
            return ctypes.windll.shell32.IsUserAnAdmin()
        except:
            return False

    if is_admin():
        admin_only()
    else:
        # Re-run the program with admin rights
        ctypes.windll.shell32.ShellExecuteW(None, "runas", sys.executable, sys.argv[0], None, 1)

if __name__ == '__main__':
    run_as_admin()