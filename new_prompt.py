import subprocess
import os
import sys
import ctypes

def new_prompt_bat():
    """
    Opens a new prompt and executes a -bat file.
    /k option prevents prompt closing after .bat  finishes.
    Alternativelly, adding "pause to end of called scipt does +- the same"
    """
    # get .bat file in current directory
    curr_dir = os.path.abspath(os.path.dirname(__file__))
    my_bat = os.path.join( curr_dir, "hello_world.bat")
    print( my_bat )

    # Launch script in new prompt
    subprocess_cmd = [ "start", "cmd", '/k']
    subprocess_cmd.append( my_bat )
    DETACHED_PROCESS = getattr(subprocess, 'DETACHED_PROCESS', 0x00000008)

    results = subprocess.Popen( subprocess_cmd , 
                                shell=True,
                                close_fds=True,
                                creationflags=DETACHED_PROCESS )
    
def new_promt_admin():
    """
    Open a new prompt with elevated priviledges
    """
    try:
        admin_name = input(r'please enter DOMAIN\admin credentials: ')
        
        subprocess_cmd = []
        subprocess_cmd.append( 'runas') 
        subprocess_cmd.append( r'/user:'+admin_name )
        subprocess_cmd.append( r'cmd /k' )

        DETACHED_PROCESS = getattr(subprocess, 'DETACHED_PROCESS', 0x00000008)
        results = subprocess.Popen( subprocess_cmd , 
                                    close_fds=True,
                                    creationflags=DETACHED_PROCESS )

    except Exception as e:
        print(e)

if __name__ == '__main__':
    #new_promt_admin()
    new_prompt_bat()