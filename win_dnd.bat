@echo off
setlocal ENABLEDELAYEDEXPANSION
rem https://stackoverflow.com/questions/5185030/drag-and-drop-batch-file-for-multiple-files/5192427#5192427
rem *** Take the cmd-line, remove all until the first parameter
rem *** Copy cmdcmdline without any modifications, as cmdcmdline has some strange behaviour
set "params=!cmdcmdline!"
set "params=!params:~0,-1!"
set "params=!params:*" =!"
set count=0

rem Split the parameters on spaces but respect the quotes
for %%G IN (!params!) do (
  set /a count+=1
  set "item_!count!=%%~G"
  rem echo !count! %%~G
)
echo "test"
rem store parameters into list
for /L %%n in (1,1,!count!) DO (
  set vector[%%n]=!item_%%n!
  rem echo %%n #!item_%%n!#
)

for /L %%i in (0,1,!count!) do (
   echo !vector[%%i]!
)
echo "test done"
rem set VAR_1=this
rem set VAR_2=that

rem python your_script.py %1 %VAR_1% %VAR_2%
rem python
pause


REM ** The exit is important, so the cmd.exe doesn't try to execute commands after ampersands
exit
