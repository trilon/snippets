
"""
 cli autocompletion for windows with msvcrt
 as readline is not meant to work on windows
"""

import msvcrt
import sys
import re
from os import system
from os import path
from shutil import get_terminal_size


allowed_chars = r'\w*\.*'
reserved = [  b'\xe0', # ARROWS
                b'\x1b', # ESC
                b'\x83',
                b'\xdd',# ¦
                b'\xf8',# °
                b'\xf5',# §
                b'\xaa',# ¬
                b'\xbd',# ¢
                b'\x00',#
                b'\xbd',# ¢
                b'\x80',# ¢
                b'\x81',#
                b'\x82',# ´
                b'\xbf',# ~.
                b'\t',
                b'\r',  # carriage return
                b'\n',  # new line
                b'\x08',# backspace
             ]


cli_w =  get_terminal_size()[0]-1

print('*'*cli_w)
print('cli autocompletion test for windows:')
print('*'*cli_w)

def print_candidates( candidates ):
    """
    print candidates the fancy way:
    """
    # get longest string in candidates
    candidates_longest = len(max(candidates, key=len))+2
    candidates.sort()

    print()
    print('-'*int(cli_w/2))
    print('-Candidates:')
    
    line =''
    pad = '{:'+str(candidates_longest)+'}'
    for w in candidates:
        line += pad.format( w )
        if len(line) >= cli_w:
            print()
            line= pad.format( w )
        print( pad.format( w ) , end='')
    print()
    
    print('-'*int(cli_w/2))


def overwrite( msg ):
    # erase the line, remove one char and reprint message
    sys.stdout.write( '\r'+' '*(cli_w-1) )
    sys.stdout.flush()
    sys.stdout.write( '\r'+msg )
    sys.stdout.flush()

def autocomplete( candidates ):
    print_candidates( candidates )

    print('press ctrl c to quit')
    msg = ''
    idx_match =0
    matched = candidates
    while 1:
        input_char = msvcrt.getch()
        #print( '->',input_char)
        if input_char not in reserved:
            char = input_char.decode('utf-8')
            #print( '->',char)

            if re.search( allowed_chars, char ):
                msg += char
                print( char , end='')
                sys.stdout.flush()

        if input_char == b'\t':
            # TAB :: autocomplete attempt
            #nb_matches =0 # autocomplete on unique match
            #matched.clear()
            matched=[]
            idx_match =0
            for p in candidates:
                # get potential matches 
                if p.startswith( msg):
                    matched.append(p)


            if len( matched ) ==1:
                # autocomplete on same line if unique match
                msg = matched[0]
                overwrite( msg )

            else:
                # find common prefixes between matches;
                # autocomplete message with common prefix if any
                tmp_msg = path.commonprefix( matched)
                if len( tmp_msg)>0:
                    msg = tmp_msg
                if len( matched )>0:
                    print_candidates( matched )
                elif len( matched )==0:
                    overwrite( '\nNo candidates !\n')
                    print_candidates( candidates )

                print( msg, end ='')
                sys.stdout.flush()

        if input_char == b'\x08':
            # 'BACKSPACE'
            msg = msg[:-1]
            overwrite( msg )


        if input_char == b'\xe0':
            ## ARROW: UP/DOWN
            input_char = msvcrt.getch()

            if len( matched )>0:
                if input_char == b'P':
                    idx_match = (idx_match+1) % len( matched )
                    #print( '-dn', idx_match,'/', len( matched), end=' ')
                    #print( matched[ idx_match])
                    msg = matched[ idx_match]
                    overwrite( msg )

                elif input_char == b'H':
                    idx_match -=1
                    if idx_match < 0:
                        idx_match = len( matched )-1
                    #print( '-up', idx_match,'/',len( matched), end=' ')
                    #print( matched[ idx_match])
                    msg = matched[ idx_match]
                    overwrite( msg )

        if input_char == b'\x03':
            # CTRL C :: exit
            sys.exit()

        if input_char == b'\r':
            # ENTER :: resturn message
            if msg in candidates:
                print()
                return msg
            else:
                print('\nmatching = false ->'+ msg+'<-' )
                print_candidates( candidates)
                print(msg, end='')
                sys.stdout.flush()

if __name__ == '__main__':
    """candidates = ['sauerkraut','test', 'tether', 'term',
            'spam', 'terminator', 'thermador', 'temodar',
            'bacon & eggs', 'beans with ham',
            'termination', 'termistor']
    """
    candidates = [ 
                    'test_FR_vert.xml',
                    'test_FR_horiz.xml',
                    'test_FR_portrait.xml',

                    'test_CN_vert.xml',
                    'test_CN_horiz.xml',
                    'test_CN_portrait.xml',

                    'test_CY_vert.xml',
                    'test_CY_horiz.xml',
                    'test_CY_portrait.xml',

                    'final_FR_vert.xml',
                    'final_FR_horiz.xml',
                    'final_FR_portrait.xml',

                    'final_CN_vert.xml',
                    'final_CN_horiz.xml',
                    'final_CN_portrait.xml',

                    'Final_CY_vert.xml',
                    'Final_CY_horiz.xml',
                    'Final_CY_portrait.xml',

                    ]


    msg = autocomplete( candidates )
    print ('--->',msg)

